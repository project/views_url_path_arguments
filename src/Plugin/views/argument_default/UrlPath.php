<?php

declare(strict_types=1);

namespace Drupal\views_url_path_arguments\Plugin\views\argument_default;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\path_alias\AliasRepositoryInterface;
use Drupal\views\Attribute\ViewsArgumentDefault;
use Drupal\views\Plugin\views\argument_default\ArgumentDefaultPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Convert an entity id to its url path.
 */
#[ViewsArgumentDefault(
  id: 'views_url_path',
  title: new TranslatableMarkup('Entity ID converted from URL path alias'),
)]
class UrlPath extends ArgumentDefaultPluginBase implements CacheableDependencyInterface {

  public function __construct(
    array $configuration,
    $pluginId,
    $pluginDefinition,
    protected readonly RouteMatchInterface $routeMatch,
    protected readonly LanguageManagerInterface $languageManager,
    protected readonly AliasRepositoryInterface $aliasRepository,
  ) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): self {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_route_match'),
      $container->get('language_manager'),
      $container->get('path_alias.repository'),
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions(): array {
    $options = parent::defineOptions();

    $options['provide_static_segments'] = ['default' => FALSE];
    $options['segments'] = ['default' => ''];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state): void {
    $form['provide_static_segments'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Provide a static URL segment(s) to prefix aliases?'),
      '#default_value' => $this->options['provide_static_segments'],
    ];
    $form['segments'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Segments'),
      '#description' => $this->t('Without leading and/or trailing slashes.'),
      '#default_value' => $this->options['segments'],
      '#states' => [
        'visible' => [
          ':input[name="options[argument_default][views_url_path][provide_static_segments]"]' => ['checked' => TRUE],
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function validateOptionsForm(&$form, FormStateInterface $form_state): void {
    $values = $form_state->getValue($form['#parents']);
    if (isset($values['segments']) && $values['segments'] !== trim($values['segments'], '/')) {
      $form_state->setError($form['segments'], $this->t('The URL segments must not contain a leading or trailing slash (/).'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getArgument(): string {
    $parameter = $this->routeMatch->getParameters()->all();
    $lastSegment = array_pop($parameter);

    // Is it already the entity id?
    if (ctype_digit($lastSegment)) {
      return $lastSegment;
    }

    $alias = '/';
    if ($this->options['provide_static_segments']) {
      $alias .= $this->options['segments'] . '/';
    }

    $alias .= $lastSegment;
    $langcode = $this->languageManager->getCurrentLanguage(LanguageInterface::TYPE_URL)->getId();

    $canonicalPath = '';
    if ($alias = $this->aliasRepository->lookupByAlias($alias, $langcode)) {
      $canonicalPath = $alias['path'];
    }

    return substr($canonicalPath, strrpos($canonicalPath, '/') + 1);
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge(): int {
    return Cache::PERMANENT;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts(): array {
    return ['url'];
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies(): array {
    $dependencies = parent::calculateDependencies();
    $dependencies['module'][] = 'views_url_path_arguments';
    return $dependencies;
  }

}
