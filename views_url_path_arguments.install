<?php

/**
 * @file
 * Install, update and uninstall functions for views_url_path_arguments module.
 */

declare(strict_types=1);

use Drupal\Core\Config\Entity\ConfigEntityUpdater;
use Drupal\views\ViewEntityInterface;

/**
 * Update views with correct schema values.
 */
function views_url_path_arguments_update_8101(array &$sandbox = []): void {
  $config_entity_updater = \Drupal::classResolver(ConfigEntityUpdater::class);
  $config_entity_updater->update($sandbox, 'view', function (ViewEntityInterface $view) {
    foreach ($view->get('display') as $display) {
      // Go through all the arguments on each display and find ones using
      // 'views_url_path' as the plugin for the default_argument or validation.
      if (!empty($display['display_options']['arguments'])) {
        foreach ($display['display_options']['arguments'] as $argument) {
          if ($argument['default_argument_type'] === 'views_url_path') {
            return TRUE;
          }
          if ($argument['validate']['type'] === 'views_url_path') {
            return TRUE;
          }
        }
      }
    }
    return FALSE;
  });
}
