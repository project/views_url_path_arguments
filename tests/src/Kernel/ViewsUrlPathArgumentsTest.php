<?php

declare(strict_types=1);

namespace Drupal\Tests\views_url_path_arguments\Kernel\Plugin;

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\path_alias\AliasRepository;
use Drupal\Tests\views\Kernel\ViewsKernelTestBase;
use Drupal\views\Views;
use PHPUnit\Framework\Attributes\Group;
use Symfony\Component\HttpFoundation\ParameterBag;

/**
 * Tests views_url_path_arguments argument plugins.
 */
#[Group('views_url_path_arguments')]
class ViewsUrlPathArgumentsTest extends ViewsKernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'path_alias',
    'views_url_path_arguments',
  ];

  /**
   * {@inheritdoc}
   */
  public static $testViews = ['test_view'];

  /**
   * {@inheritdoc}
   */
  protected function setUp($import_test_views = TRUE): void {
    parent::setUp($import_test_views);

    // The alias prefix list expects that the menu path roots are set by a
    // menu router rebuild.
    \Drupal::state()->set('router.path_roots', ['user', 'admin']);

    $this->installEntitySchema('path_alias');
  }

  /**
   * Tests the argument default plugin.
   */
  public function testArgumentDefaultPlugin(): void {
    $routeMatch = $this->prophesize(RouteMatchInterface::class);
    $routeMatch->getParameters()->willReturn(new ParameterBag(['one' => '1']), new ParameterBag(['chars' => 'foo']));
    $this->container->set('current_route_match', $routeMatch->reveal());
    $aliasRepository = $this->prophesize(AliasRepository::class);
    $aliasRepository->lookupByAlias('/group/foo', 'en')->willReturn(['path' => '/group/2']);
    $this->container->set('path_alias.repository', $aliasRepository->reveal());

    // Add a new argument and set the test plugin for the argument_default.
    $view = Views::getView('test_view');
    $options = [
      'default_argument_type' => 'views_url_path',
      'default_argument_options' => [
        'provide_static_segments' => TRUE,
        'segments' => 'group',
      ],
      'default_action' => 'default',
    ];
    $id = $view->addHandler('default', 'argument', 'views_test_data', 'name', $options);
    $view->initHandlers();
    $argument = $view->argument[$id];

    // Test default arguments.
    $default = $argument->getDefaultArgument();
    $this->assertEquals('1', $default);
    $default = $argument->getDefaultArgument();
    $this->assertEquals('2', $default);
  }

  /**
   * Tests the argument validator plugin.
   */
  public function testArgumentValidatorPlugin(): void {
    $aliasRepository = $this->prophesize(AliasRepository::class);
    $aliasRepository->lookupByAlias('/group/foo', 'en')->willReturn(['path' => '/group/1']);
    $aliasRepository->lookupByAlias('/group/2', 'en')->willReturn(['path' => '/group/2']);
    $aliasRepository->lookupByAlias('/group/3', 'en')->willReturn(NULL);
    $aliasRepository->lookupByAlias('/group/asdf', 'en')->willReturn(NULL);
    $this->container->set('path_alias.repository', $aliasRepository->reveal());

    // Add a new argument and set the test plugin for the argument_default.
    $view = Views::getView('test_view');
    $options = [
      'specify_validation' => TRUE,
      'validate' => [
        'type' => 'views_url_path',
      ],
      'validate_options' => [
        'provide_static_segments' => TRUE,
        'segments' => 'group',
      ],
    ];
    $id = $view->addHandler('default', 'argument', 'views_test_data', 'name', $options);
    $view->initHandlers();

    // Test argument validation.
    $argument = $view->argument[$id];
    $plugin = $argument->getPlugin('argument_validator');
    $this->assertTrue($plugin->validateArgument('foo'));
    $this->assertTrue($plugin->validateArgument('2'));
    $this->assertTrue($plugin->validateArgument('3'));
    $this->assertFalse($plugin->validateArgument('asdf'));
  }

}
